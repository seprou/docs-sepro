---
layout: mbed
title: VsCode et les projets Mbed
description: Utilisation de l'IDE VsCode pour développer sur cible embarqué mbed
keywords: mbed, linux, Vscode
toc: true
lesson: 3
---

Cette page détaille l'utilisation de VsCode et de mbed pour générer un exécutable pour ARM CortexM.  

VS code est l'IDE utilisé dans le BTS pour développer pour la cible Nucleo F746ZG.
Afin qu'il soit fonctionnel pour notre chaîne de développement, un certain nombre de paramètres doivent être personnalisés.  

Si vous avez importé un projet template depuis notre GIT, il est immédiatement fonctionnel. vous n'avez qu'à vérifier la compilation de celui-ci.  

Nous allons synthétiser l'utilisation de Vscode et détailler la configuration du projet template proposé.

## Développer avec VSCODE

### Créer un projet à partir d'un projet template

Maintenant que la chaine de compilation est installée, vous allez pouvoir tester la compilation, le téléversement et l'exécution de votre projet

1.   Cloner le projet template 

Cas Linux

~~~shell
git clone https://gitlab.sn-kastler.fr/modeles/mbed6_template.git <nomProjet>
~~~

Cas Windows

~~~shell
git clone -b Windows https://gitlab.sn-kastler.fr/modeles/mbed6_template.git <NomProjet>
~~~

1.   Se déplacer dans le dossier

~~~shell
cd <nomProjet>
~~~

1.   Installer mbed et les librairies à partir des fichiers *.lib

**Attention**, La commande mbed update télécharge + de 1Go de données la première fois...Soyez patient !
{: .alert .alert-warning}

~~~shell
mbed update
~~~

### Ouvrir le projet dans vscode

> Fichier -> Ouvrir un dossier

### Compiler et exécuter un programme

Dans Vscode

> Executer -> Exécuter sans débogage

Le projet se compile (plusieurs minutes la première fois, puis est téléversé sur la carte.
{: .alert .alert-info}

Lors du premier téléversement, une fenêtre s'ouvre pour confirmer que vous voulez utiliser JLink. Cocher la case "Do not show this message again" et cliquer sur **Accept**
{: .alert .alert-warning}

![Jlink]({{ 'pub/images/mbed/jlink_term_of_use.png' | absolute_url }} "Jlink term of use"){: .shadow}{: height="400px"}

### Exécuter le programme

Une fois le programme téléversé sur la carte, il attend que vous l'exécutiez

![Vscode en attente]({{ 'pub/images/mbed/Vscode_premier_lancement.png' | absolute_url }} "Vscode en attente"){: .shadow}{: height="400px"}

> Cliquer sur la flèche bleu pour lancer le programme

![Flèche exec]({{ 'pub/images/mbed/vscode_debug.png' | absolute_url }} "Flèche exec"){: .shadow}{: width="200px"}

### Rediriger la sortie écran.

La sonde intégrée Jlink permet de rediriger la sortie écran vers un terminal (`/dev/ttyACM0`)

Pour cela,

* Cliquer sur l'onglet terminal de VSCODE.
* Entrer la commande suivante

Si `screen` n'est pas installé faite-le !
{: .alert .alert-info}

~~~ shell
screen /dev/ttyACM0
~~~

ou bien si vous utilisez `cu`

~~~ shell
cu -l /dev/ttyACMO
~~~

Dans le programme, les printf() seront redirigés vers ce terminal série.

~~~ cpp
printf("LO WORLD \r\n");
~~~

N'oubliez pas de préciser `\n\r` dans votre code pour revenir à la ligne !
{: .alert .alert-warning}

Pour quitter screen

> Ctrl-A k

### Ajouter une librairie ou une classe

* Cas d'une librairie existante

Si vous ajoutez une librairie de composant avec `mbed add`, elle est immédiatement fonctionnelle.

* Cas d'une nouvelle librairie/classe

Pour que votre classe soient correctement compilée, vous devez indiquer son chemin depuis la racine de votre projet.


 **Exemple : classe CMoteur** 

1.   Créer un dossier CMoteur à la racine de votre projet
1.   Créer un fichier CMoteur.h et CMoteur.cpp dans le dossier CMoteur
1.   Inclure la classe CMoteur dans le fichier concerné (main.cpp par exemple)

     ~~~ cpp
     #include "CMoteur/Cmoteur.h"
     ~~~
 
## Projet Mbed6Template

Le template est hébergé sur notre git, [Mbed6Template]  

Vous pouvez le cloner comme indiqué ci-dessous

~~~shell
git clone https://gitlab.sn-kastler.fr/modeles/mbed6_template.git
~~~

[Mbed6Template]: https://gitlab.sn-kastler.fr/modeles/mbed6_template


### Fichiers et dossiers importants du template 

~~~ shell
.
├── main.cpp
├── mbed_app.json
├── mbed_config.h
├── mbed-os.lib
├── readme.md
├── STM32F7x6.svd
└── .vscode
    ├── c_cpp_properties.json
    ├── .cortex-debug.peripherals.state.json
    ├── .cortex-debug.registers.state.json
    ├── launch.json
    ├── settings.json
    └── tasks.json
~~~

### Répertoire principal

#### main.cpp

Le fichier main.cpp contient un "Hello World" qui allume et éteint la led1 toutes les 100ms

~~~ c
#include "mbed.h"

#define SLEEP_TIME 100ms // (msec)
DigitalOut led1(LED1);

int main() {
  while (true) {
    printf("LO world \r\n");
    led1 = !led1;
    ThisThread::sleep_for(SLEEP_TIME);
  }
}
~~~


#### mbed-app.json
Ce fichier est le fichier de configuration des options spécifques à certaines cartes.  
Ici, nous précisons que nous voulons activer l'affichage des nombres réels pour toutes les cartes.

~~~
{
    "target_overrides": {
 	"*": {
            "target.printf_lib": "minimal-printf",
            "platform.minimal-printf-enable-floating-point": true,
            "platform.minimal-printf-set-floating-point-max-decimals": 6,
            "platform.minimal-printf-enable-64-bit": false
        },
        "K64F": {
            "platform.stdio-baud-rate": 9600
        }
    }
}
~~~

#### mbed-os.lib
Le fichier contient un lien vers la version de mbed-os qui doit être utilisé par le projet.
Le fichier, comme tous les .lib est utilisé lorsqu'on exécute `mbed update`

Dans notre cas, la version téléchargée est la `6.15.0`

~~~ shell
https://github.com/ARMmbed/mbed-os/#4cfbea43cabe86bc3ed7a5287cd464be7a218938
~~~

#### STM32F7x6.svd
Ce fichier est le fichier de définition de la carte Nucleo STM32F746ZG. Il contient l'adresse de tous les registres mémoires. Il est utilisé pour voir l'état des registres lors du débogage du projet.

### Sous répertoire .vscode

Ce sous répertoire contient la configuration utilisé par VsCode pour la chaine de cross compilation
Il est indispensable au projet et dépend de la configuration des outils installé sur le PC.

#### Fichier c_cpp_properties.json
Ce fichier contient les directives de compilation propre à la carte et à l'environnement.
Je vais détailler certaines des options.

* `configurations` : Détecte si l'OS est Windows ou Linux et adapte le chemin des outils en fonction.
* `includePath` Le chemin d'inclusion automatique des bibliothèques. Ici `"${workspaceFolder}/**"` signifie "tous les sous répertoires du répertoire de travail
* `compilerPath` : Le chemin (absolu si problème) vers le compilateur.
* `cStandard` et `cppStandard` : La version de C et CPP utilisé.
* `intelliSenseMode` : Coloration syntaxique et complétion automatique de VsCode

~~~
{
    "configurations": [
        {
            "name": "Windows",
            "includePath": [
                "${workspaceFolder}/**"
            ],
            "defines": [
                //suite de définition pour Windows
            ],
            "compilerPath": "arm-none-eabi-g++",          
            "cStandard": "c11",
            "cppStandard": "c++17",
            "intelliSenseMode": "gcc-x64"
        },
        {
            "name": "Linux",
            "includePath": [
                "${workspaceFolder}/**"
            ],
            "defines": [
                "DEVICE_SPI=1",
                "TARGET_STM32F746xG",
              //suite de définition pour linux
                "\"_DEBUG\",",
                "\"UNICODE\",",
                "\"_UNICODE\""
            ],
            "compilerPath": "/opt/gcc-arm-none-eabi-9-2019-q4-major/bin/arm-none-eabi-g++",
            "cStandard": "c11",
            "cppStandard": "c++17",
            "intelliSenseMode": "gcc-x64"
        }
    ],
    "version": 4
}
~~~

#### launch.json

Ce fichier contient la configuration du téléversement et de la sonde de Debug

* `cortex-debug` : L'appel au plugin cortex-debug pour gérer le téléversement.
* `executable` : Le chmein vers l'exécutable 
* `serverpath` : Le chemin vers l'exécutable de debogage. Nous utilisons Jlink
* `gdbpath` : Le chemin vers le debugger gdb

<script src="https://gitlab.sn-kastler.fr/modeles/mbed6_template/-/snippets/17.js"></script>


#### Task.json
Ce fichier contient les directives de compilation.  

Nous utilisons mbed pour compiler.

~~~
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "name": "mbed",
    "isShellCommand": true,
    "showOutput": "always",
    "problemMatcher": {
        "owner": "cpp",
        "fileLocation": ["relative", "${workspaceRoot}"],
        "pattern": {
            "regexp": "^(.*):(\\d+):(\\d+):\\s+(warning|error):\\s+(.*)$",
            "file": 1,
            "line": 2,
            "column": 3,
            "severity": 4,
            "message": 5
        }
    },
    "args": ["compile", "--profile=debug", "-t", "GCC_ARM", "-m", "NUCLEO_F746ZG"],
    "linux": {
        "command": "mbed"
    },
    "osx": {
        "command": "mbed"
    },
    "windows": {
        "command": "mbed"
    }
}
~~~

