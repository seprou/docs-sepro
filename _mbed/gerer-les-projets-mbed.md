---
layout: mbed
title: Gérer les projets mbed avec mbed-cli
description: mbed cli est l'interface en ligne de commande qui est utilisé pour gérer les projets
keywords: mbed, linux, mbed-cli, cross compilation
toc: true
lesson: 2
---

mbed-cli est l'interface en ligne de commande utilisé pour gérer les projets mbed. Nous allons résumer ci-après les principales commandes utilisées.

## Principales commandes mbed_cli

- Vérifier l'installation

~~~shell
mbed --help
~~~

#### Configuration de l'environnement

- Configurer le PATH de gcc-arm (configuration globale)

~~~shell
mbed config -G ARM_PATH /usr/bin
~~~

- Lister la configuration (Se placer dans le dossier du projet)

~~~shell
mbed config --list
~~~

- Supprimer une configuration

**Exemple** : Suppression de la configuration MBED_OS_DIR en global

~~~shell
mbed config -G --unset MBED_OS_DIR
~~~

#### Vérification et mise à jour de la version

- Vérifier la version de mbed. 

**Information** : La sortie de mbed releases pointe sur le tag courant de la version
{: .alert .alert-info}

~~~shell
cd mbed-os
mbed releases
~~~

- Mise a jour de mbed-os (exemple ici en 6.15.1)

~~~shell
cd mbed-os
mbed update 6.15.1
~~~

#### Gestion des versions du projet

- Publier son travail 

**Information** : Soit vous pouvez faire **git add**, **git commit** et **git push**, soit vous utilisez la commande mbed pour ces 3 actions
{: .alert .alert-info}

~~~shell
mbed publish 
~~~

## Librairies de composants

mbed permet récupérer les librairies d'éléments matériels supplémentaires et de les intégrer à un projet 

#### Gérer les librairies : Cas général 

* Ajouter une librairie de composant

~~~shell
mbed add <adresse librairie>
~~~

* Lister les librairie installées

~~~shell
mbed ls -a
~~~

- Supprimer une librairie existante

~~~shell
mbed remove <nom_librairie> 
~~~

#### Exemple de la librairie mbed-cloud-client

- Ajouter la librairie au projet 

~~~shell
mbed add https://github.com/ARMmbed/mbed-cloud-client
~~~

- Lister les librairies du projet

{::options parse_block_html="true" /}
<details>
  <summary markdown="span">mbed ls </summary>

~~~shell
[mbed] Working path "~/tmp/Mbed6_Template_JLink" (program)
Mbed6_Template_JLink (#dc524271d409)
|- mbed-cloud-client (#8a30590179cd, tag: 4.11.1)
`- mbed-os (#4cfbea43cabe, tags: mbed-os-6.15.0, mbed-os-6.15.0-rc3)      
~~~

</details>

{::options parse_block_html="true" /}
  


- Enlever la librairie du projet

~~~shell
mbed remove mbed-cloud-client        
~~~

## Librairie de composants
Quelques exemples de composants matériels à intégrer à vos projets. Vous trouverez ci-dessous les plus courants avec la documentation et les librairies associés.

#### Librairie sonde BME280 (BME280t)

- [Documentation BME280](https://os.mbed.com/components/BME280-Combined-humidity-and-pressure-se/) 

- Importation de la librairie

~~~shell
mbed add http://os.mbed.com/users/MACRUM/code/BME280/
~~~

#### Librairie Capteur CO2 MQ135 

- [Documentation MQ135](https://projetsdiy.fr/mq135-mesure-qualite-air-polluant-arduino/)
- Documentations suplémentaires
  - https://os.mbed.com/users/readysteadygo2006/code/MQ135/  
  - https://github.com/ViliusKraujutis/MQ135  
  - http://davidegironi.blogspot.com/2014/01/cheap-co2-meter-using-mq135-sensor-with.html  

* Importation de la librairie

~~~shell
mbed add http://os.mbed.com/users/sepro/code/MQ-135/
~~~

## Gérer plusieurs projets Mbed

Quand on gère plusieurs projets mbed, cela devient lourd de télécharger à chaque fois une version de mbed-os pour tous ces projets.

Il est possible de télécharger le dossier mbed-os une fois sur son poste et de faire référence à ce dossier dans son projet.

* Choisir un emplacement pour importer mbed-os
* Importer mbed-os
* Ajouter le chemin dans la configuration globale (MBED_OS_DIR)

~~~ shell
cd /absolute/path/to/project/dir
$ mbed import mbed-os
$ mbed config -G MBED_OS_DIR /absolute/path/to/project/dir/mbed-os
[mbed] /absolute/path/to/project/dir/mbed-os now set as global MBED_OS_DIR
~~~

Dans le projet, modifier la ligne de compilation dans le fichier task.json en ajoutant dans source le chemin vers mbed-os

~~~ json
// Ancienne ligne
// "args": ["compile", "--profile=debug", "-t", "GCC_ARM", "-m", "NUCLEO_F746ZG"],

// Nouvelle ligne
"args": ["compile", "--source=.", "--source=/absolute/path/to/project/dir/mbed-os", "--profile=debug", "-t", "GCC_ARM", "-m", "NUCLEO_F746ZG"],
~~~

