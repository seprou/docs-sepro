---
layout: mbed
title: Installation de la chaine de cross-compilation
description: Pour développer sur une cible embarqué, il faut mettre en place une chaine de cross-compilation
keywords: mbed, linux, Jlink, STM32, Nucleo-F746ZG
toc: true
lesson: 1
---

## Installation de la chaine de développement croisée

Nous utilisons les platines [Nucleo-F746ZG].
Le tutorial ci-après vous guide dans l'installation de [MBED] et [Visual Studio Code]( https://code.visualstudio.com/) sous Ubuntu/Debian pour ces platines.


Les platines nucleo possèdent un microcontroleur ARM Cortex M qui est compatible avec MbedOs.

Pour pouvoir les utiliser, il faut avant tout mettre en place la chaine de compilation croisée.

Nous utiliserons : 

* l'IDE Visual Studio Code pour programmer
* Le compilateur gcc-arm pour obtenir un exécutable compatible avec la carte nucléo.
* Le programmateur JLink pour téléverser et débugger le programme.

Le tutoriel ci-après est adapté de celui-ci

[wiki.segger.com/J-Link:Visual_Studio_Code](https://wiki.segger.com/J-Link:Visual_Studio_Code){:target="_blank"}

## Passer de STLink en Jlink

Par défaut, les cartes nucléo sont fournies avec le programmateur StLink. Nous avons choisi de les modifier pour pouvoir utiliser JLink.
Dans le cas d'une nouvelle carte (personnelle par exemple), il faut flasher le firmware pour la rendre compatible JLink. Suivre le tutoriel ci-dessous.

[STLink to JLink](https://www.segger.com/products/debug-probes/j-link/models/other-j-links/st-link-on-board/)

## Installation de la chaîne de compilation croisée

La chaine de compilation croisée va permettre de générer un exécutable pour le processeur de notre carte (ARM Cortex M)

~~~shell
sudo apt install gdb-multiarch libusb-1.0-0-dev gawk gcc
sudo apt install libffi-dev libncurses5
~~~

**Attention**, l'installation du compilateur dépend de votre OS
{: .alert .alert-warning}


#### Ubuntu 18.04 
Il faut télécharger le paquet ci-dessous

~~~shell
sudo apt install gcc-arm-none-eabi
~~~

#### Ubuntu 20.04 et Debian10 
Il faut télécharger la version de arm-gcc GNU Arm Embedded version 9-2019-q4-major

1.   [Télécharger gcc-arm-none-eabi-9-2019-q4-major-x86_64](https://developer.arm.com/-/media/Files/downloads/gnu-rm/9-2019q4/gcc-arm-none-eabi-9-2019-q4-major-x86_64-linux.tar.bz2?revision=108bd959-44bd-4619-9c19-26187abf5225&la=en&hash=E788CE92E5DFD64B2A8C246BBA91A249CB8E2D2D){:target="_blank"}

1.   Décompresser dans /opt

     ~~~shell
     sudo mv file.tar.bz2 /opt
     cd /opt && sudo tar xjf file.tar.bz2
     ~~~

1.   Ajouter les chemins au fichier ~/.bashrc

     1.    Ouvrir le fichier .bashrc

           ~~~shell
           nano ~/.bashrc
           ~~~

     1.   Ajouter la ligne ci-dessous à la fin du fichier    

          ~~~shell
          export PATH=$PATH:/opt/gcc-arm-none-eabi-9-2019-q4-major/bin:/opt/gcc-arm-none-eabi-9-2019-q4-major/arm-none-eabi/bin:/opt/gcc-arm-none-eabi-9-2019-q4-major/arm-none-eabi/lib
          ~~~

1.   Sauvegarder le fichier (CTRL+O, CTRL+X)

1.   Recharger le fichier .bashrc

     ~~~shell
     source .bashrc
     ~~~

## mbed_cli

Mbed cli est une suite logicielle en ligne de commande pour gérer les projets Mbed

#### Installation de mbed_cli

1.   Installer les paquets nécessaires

     ~~~shell
     sudo apt install python3 python3-pip git mercurial
     ~~~

     ~~~shell
     python3 -m pip install mbed-cli
     ~~~

1.   Ajouter le chemin de l'exécutable dans le PATH

     1.   Ouvrir ~/.bashrc

          ~~~shell
          nano ~/.bashrc
          ~~~

     1.   Ajouter la ligne ci-dessous à la fin du fichier

          ~~~shell
          export PATH="$HOME/.local/bin/:$PATH"
          ~~~

     1.   Sauvegarder le fichier et recharger la configuration

          ~~~shell
          source ~/.bashrc
          ~~~

1.   Vérifier l'installation de mbed cli

     ~~~shell
     mbed --version
     >1.10.1 (ou supérieur)
     ~~~

**Attention**, Si vous n'obtenez pas la version du logiciel (exemple, mbed not found), recommencer la procédure
{: .alert .alert-danger}


#### Configurer mbed cli

Il faut configurer le PATH de gcc-arm dans la configuration pour tous vos projets Mbed

- Ubuntu 18.04

~~~shell
mbed config -G ARM_PATH /usr/bin
~~~

- Ubuntu 20.04 et debian 10

~~~shell
mbed config -G GCC_ARM_PATH /opt/gcc-arm-none-eabi-9-2019-q4-major/bin
~~~

## Visual Studio Code (VsCode)

VsCode est l'IDE utilisé pour programmer.

#### Installation 

1.   Importer la clé GPG de microsoft

     ~~~shell
     wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
     ~~~

1.   Ajouter le dépot de vscode

     ~~~shell
     sudo apt-get install software-properties-common
     sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
     ~~~

1.   Installer vscode

     ~~~shell
     sudo apt update``sudo apt install code
     ~~~

##### Installation des plugins **C/C++ IntelliSense** et **Cortex-Debug**

Pour installer les plugins, il faut ouvrir VsCode, aller dans plugins, télécharger et activer les plugins ci-dessus 

![VsCode]({{ 'pub/images/mbed/vscode-plugins.png' | absolute_url }} "VsCode et Plugin"){: .shadow}{: height="400px"}

## Sonde Jlink

La sonde Jlink nous permet de téléverser et de debugguer les programmes sur la carte nucléo

1.   [Télécharger la dernière version du Jlink Software for Linux v64bits (v680c ou plus)](https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack){:target="_blank"} 

1.   Installer le .deb

     ~~~shell
     sudo apt install ./JLink_Linux_V680c_x86_64.deb
     ~~~



## Outils et ressources documentaires

[API Mbed OS 6.15](https://os.mbed.com/docs/mbed-os/v6.15/introduction/index.html)

### Documentation STM32-F746

[Nucleo Databrief]({{ 'pub/pdf/stm32/nucleo-f746zgDataBrief.pdf' | absolute_url }})

[Nucleo UserManual]({{ 'pub/pdf/stm32/Nucleo-F746ZG_UserManual.pdf' | absolute_url }})

[STM32F746]({{ 'pub/pdf/stm32/stm32f746zg.pdf' | absolute_url }})

[CortexM7-peripherals]({{ 'pub/pdf/stm32/CortexM7_peripherals.pdf' | absolute_url }})

[CortexM7 registers]({{ 'pub/pdf/stm32/CortexM7_registers.pdf' | absolute_url }})


[MBED]: https://os.mbed.com/
[Nucleo-F746ZG]: https://os.mbed.com/platforms/ST-Nucleo-F746ZG/

