---
layout: page
title: A propos
toc: false
---

<script src="https://kit.fontawesome.com/245306b796.js" crossorigin="anonymous"></script>

<p class="message" style="align:center">
<div class="sidebar-logo" style="align:center">
    <img src="https://www.gravatar.com/avatar/{{ site.author.gravatar_md5 }}?s=120" />
</div>
  Sébastien Prouff
</p>



Ce blog rassemble les informations techniques glanés ici ou là, le plus souvent à partir d'autres ressources Internet.

Comme je suis enseignant en informatique, mes recherches concernent le plus souvent l'appropriation de ces ressources techniques à but pédagogique.

Donc, il y a aussi des considérations pédagogiques au fil des pages.



