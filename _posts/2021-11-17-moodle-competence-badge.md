---
layout: post
title: Moodle, compétences et badges
description: Comment utiliser Moodle pour gérer les compétences du référentiel et attribuer des badges aux étudiants ?
keywords: Moodle SNIR référentiel badge
toc: true
---

Motiver ces étudiants est un défi quotidien pour un prof !  
Nous avons un public hétérogène, issu de formations diverses, comme leurs capacités et compétences.

Au quotidien, nous utilisons [Moodle](https://moodle.com/fr/) comme plateforme LMS.
Les capacités de Moodle sont incroyables, je ne reviendrais pas dessus. Par contre, son interface est parfois obscure, les menus étant perdus dans des sous menus de sous menus, les options infinies et parfois peu compréhensibles. 

Depuis quelques temps, je voulais mettre en place un système qui attribue des badges aux étudiants lorsque ils  valident une compétence du référentiel au travers un plan de formation.

C'est désormais en place, voici les différentes étapes à suivre pour y arriver !
Le tutoriel ci-dessous prend comme exemple notre référentiel, mais il est adaptable pour toutes les formations.

## Pourquoi ?
Je forme des techniciens supérieurs en Systèmes Numérique. Les cours dispensés couvrent les objectifs du référentiel, et ces objectifs sont liés à des compétences métiers attendus par les entreprises lors du recrutement du technicien supérieur ayant obtenu le BT SNIR.
Par contre, au quotidien, pour les étudiants, les objectifs de formations sont insuffisamment explicites. 

En les associant à un plan de formation, je veux : 
* Les responsabiliser
* qu'ils aient une vision claire des compétences acquises et non acquises

Un plan de formation aide les étudiants à vivre activement et positivement leur formation.

En effet, contrairement aux notes, souvent vécus comme une sanction, si la compétence n'est pas acquise à un instant donné, il sera toujours possible de la valider lors d'une autre activité ou un autre module.

Enfin, pour donner une dimension ludique, les badges sont utilisés et associés aux compétences acquises.

[Les badges, gadget ?](https://www.unow.fr/blog/le-coin-des-experts/badge-a-competence)

## Liens vers la documentation Moodle

[Référentiel de compétences](https://docs.moodle.org/3x/fr/R%C3%A9f%C3%A9rentiels_de_comp%C3%A9tences)

[Plan de formation](https://docs.moodle.org/3x/fr/Plans_de_formation)

[Badges](https://docs.moodle.org/3x/fr/Badges)


## Définition du référentiel de compétence.

Dans un premier temps, il faut définir votre référentiel de compétences en tant qu'administrateur du site.
[Référentiels de compétences](https://docs.moodle.org/3x/fr/R%C3%A9f%C3%A9rentiels_de_comp%C3%A9tences)

~~~
Accueil
->Administration du site
->Compétences
->Référentiels de compétences
~~~

![moodle referentiel]({{ 'pub/images/moodle/referentiel_competences.png' | absolute_url }}){: .shadow}

Dans cette partie, il faut ajouter chacune des compétences du référentiel, des sous compétences ainsi que les niveaux taxonomiques à atteindre.
La capture d'écran ci-dessus montre que par exemple j'ai défini deux niveaux (N1 et N2) pour la compétence `C4.3- Installer et configurer une chaine de développement`

En effet, pour motiver les étudiants tout au long de leur formation, il faut qu'ils puissent tous atteindre certains niveaux. De plus, les mêmes compétences sont approfondies dans plusieurs modules. Ils peuvent très bien obtenir le niveau N1 avec moi en première année et valider leur niveau N2 avec un collègue en deuxième année.

> Astuces !  
On peut définir une compétence comme acquise quand les compétences filles sont acquises.  
Pour cela, Dans le référentiel de compétence, 
* sélectionner la compétence "mère", 
* puis Modifier-> Règles de compétences
* Choisir que l'objectif est marqué comme atteint Quand Toutes les compétences filles sont atteintes

![Compétences atteintes]({{ 'pub/images/moodle/competence-atteinte.png' | absolute_url }}){: .shadow}



## Définition des modèles de plan de formation

Ensuite, il faut associer les compétences à un plan de formation.
Dans notre cas, nous avons des **Taches Professionnelles** définies dans le référentiel qui associent les compétences à celles-ci.

Par exemple la tache `Réaliser et valider une solution` nécessite de savoir `Documenter une réalisation matérielle et logicielle`, mais également d'`Intégrer un module logiciel` ou encore de `Tester et valider un module logiciel`

Bref, dans Moodle, 

* dans le menu

    ~~~
    Accueil
    ->Administration du site
    ->Compétences
    ->Modèles de plan de formation 
    ~~~

* Il faut définir les plans de formations

![moodle plan de formation]({{ 'pub/images/moodle/modele-plan-formation.png' | absolute_url }}){: .shadow}

* Et associer les compétences voulues à chacun des plans

![moodle tache professionnelle]({{ 'pub/images/moodle/tache_pro.png' | absolute_url }}){: .shadow}

* Ensuite, il faut ajouter la cohorte d'étudiants à un plan de formation pour qu'il soit disponible. Les étudiants retrouvent leurs plans de formation dans leur profil utilisateur.

![Etudiant - Plan de formation]({{ 'pub/images/moodle/etudiant-plan-de-formation.png' | absolute_url }}){: .shadow}

## Créer et gérer les badges

Les badges choisis dans ce tutoriel sont des badges de site. Comme nous sommes plusieurs enseignants à intervenir dans plusieurs modules, il est préférable d'associer les badges au site et non au cours.

Pour créer des badges de site, il faut avoir les droits. Le lien sur la documentation de Moodle ci-dessus détaille la procédure.

Pour créer les badges, j'utilise le site [badge Design](https://badge.design/)  
On peut choisir et créer ses badges en 5 minutes comme l'exemple ci-dessous.

![Exemple de badge]({{ 'pub/images/moodle/C4.4-Developper-module-logiciel.png' | absolute_url }}){: .shadow}

Une fois le badge créé, on se déplace dans le menu.

~~~
Accueil->Administration du site->Badges->Gérer les badges-> Ajouter un badge
~~~

En ce qui me concerne, je souhaite que les badges soient automatiquement attribués quand la compétence est atteinte.
Lors de la création du badge, j'associe la compétence à l'obtention du badge.

![Gérer les badges]({{ 'pub/images/moodle/Gerer-badges.png' | absolute_url }}){: .shadow}


## Associer les compétences à un cours

Ça y est, le squelette est en place.  
Vous avez un référentiel, un plan de formation, des badges.  
Maintenant, il faut gérer le cours pour pouvoir valider les compétences dans celui-ci, automatiquement ou non en fonction de vos activités.

### Paramétrer le cours
En préalable, dans les paramètres du cours, activer l'achèvement d'activité dans les paramètres du cours.
Cela permettra de pouvoir achever une activité et de déclencher une action lors de l'achèvement de celle-ci.

~~~
Paramètres -> Suivi d'achèvement -> Activer le suivi de l'achèvement des activités 
~~~

### Ajouter des compétences au cours.

Dans le menu à gauche, 
* Ajouter des compétences au cours.
* Choisissez de reporter immédiatement les évaluations de compétences de ce cours aux plans de formation. 

### Associer une compétence à une activité

Lorsque vous créez une activité, il faut activer le suivi d'achèvement de l'activité.

Ensuite, dans la partie compétence, vous choisissez la ou les compétences associées à cette activité, et vous sélectionnez une option lors de l'achèvement.  
Personnellement, je choisis l'option "Envoyer pour validation". Ainsi, je vais retrouver sur mon tableau de bord, tous les étudiants qui me demande une validation pour pouvoir évaluer facilement leurs compétences.

![Activité et compétences]({{ 'pub/images/moodle/competence-activite.png' | absolute_url }}){: .shadow}

### Ajouter automatiquement une compétence lors de l'achèvement d'une activité.

Vous pouvez également choisir de marquer la compétence comme atteinte lors de l'achèvement de l'activité.
Dans ce cas, il faut choisir l'option "Marquer la compétence comme atteinte".

Une petite astuce, pour que celle-ci soit automatiquement validée, il faut choisir le barème correctement dans le référentiel de compétence. (le réglage est aussi à faire dans la définition du référentiel de compétence (Cliquer sur la roue cranté pour modifier le référentiel de compétence) )
- Le barème "défaut" donne le niveau atteint lors de la validation automatique.
- Les élément "compétences acquises" sont pour une validation manuelle.

![Barèmes de compétences]({{ 'pub/images/moodle/bareme-de-competences.png' | absolute_url }}){: .shadow}


### Point de vue enseignant

Dans le tableau de bord, ajouter le bloc "Plan de formation".
Ainsi, dès qu'une compétence est à valider, elle apparaît dans ce bloc.

![Tableau de bord]({{ 'pub/images/moodle/tableau-de-bord.png' | absolute_url }}){: .shadow}

Il faut ensuite cliquer dessus et valider la compétence (ou non !)

### Point de vue étudiant.

Le plan de formation est automatiquement mis à jour avec les compétences atteintes.

![Plan de formation]({{ 'pub/images/moodle/exemple-plan-formation.png' | absolute_url }}){: .shadow}

Et les badges s'affichent dans leur profil.

![Badges]({{ 'pub/images/moodle/Etudiant-badge.png' | absolute_url }}){: .shadow}

## Conclusion

Ça y est, l'outil est en place et déployé !

J'espère que mes étudiants seront plus motivés par cette pratique. 

Comme nous sommes plusieurs enseignants à intervenir dans la formation, cela devrait aussi faciliter le suivi des étudiants entre collègues.

Le dispositif est nouveau (pour nous). Maintenant qu'il est en place, il ne reste plus qu'à le faire vivre pour un retour d'expérience et l'améliorer.

