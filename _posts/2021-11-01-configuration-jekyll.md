---
layout: post
title: Configuration du site Jekyll
description : Le site est installé et déployé sur Gitlab Pages. Maintenant, synthétisons les commandes utiles, l'ajout d'une TOC et d'une collection de pages.
keywords : bundle, TOC, collection
toc: true
---

Comme détaillé dans le précédent post, j'ai installé ce site avec Jekyll/Gitlab. Les quelques notes ci-après détaillent la configuration du SSG.


## Ruby, commande bundle et Jekyll 
Premièrement, je découvre Ruby (si si) et ma connaissance de Ruby et des Gems est proche de zéro. Donc, quelques commandes pense-bête qui m'ont servies.

### Bundler
Jekyll utilise [Bundler] et un `Gemfile`.
Il y a énormément de plugins Jekyll qui facilitent la configuration du site. Les plugins utilisés sont à déclarer dans le fichier `_config.yml`

~~~ shell
plugins:
  - jekyll-paginate
  - jekyll-seo-tag
  - jekyll-toc
~~~
On peut installer les plugins sur le système, mais il vaut mieux gérer ceux-ci dans le Gemfile avec bundle.
En plus, lors du déploiement du site, le pipeline lit le Gemfile et installe les plugins du Gemfile.
Donc, la bonne pratique est de déclarer les plugins dans le Gemfile puis de faire

~~~ shell
bundle install
~~~

### Mon site en local
Jekyll propose une commande pour exécuter un serveur web local. De plus, `jekyll serve` reconstruit automatiquement le site à chaque modification de contenu. (Sauf pour une modification dans `_config.yml` où il faut arrêter et redémarrer le serveur)

~~~ shell
jekyll serve
~~~

Le site est ensuite disponible à l'adresse : 
~~~
http://localhost:4000
~~~

On peut même forcer le rafraichissement de la page web lors de la mise à jour du contenu avec
{: .alert .alert-info}

~~~
jekyll serve --livereload
~~~


## Tips

Quelques trucs et astuces pour construire correctement le tutoriel. (et le reste des pages)

> _Prerequis_ :
>
> * Une connaissance basique du langage Markdown.
> * J'utilise [VsCode] pour le site. Il est très pratique, notamment grâce à son terminal intégré et sa fenêtre de prévisualisation.

### No Liquid (sometimes)
Pour ne pas interpréter le code [Liquid] un bloc de code.

~~~
{% raw %}{%{% endraw %} raw %}
{% raw %}{%{% endraw %} endraw %}
~~~

### Bootstrap
J'ai ajouté le css de [bootstrap] pour quelques info box. Je ne suis pas encore complétement convaincu mais, je le garde sous le coude.
Pour l'utiliser, après téléchargement et décompression, il faut ajouter le chemin vers le css dans `head.html`.

~~~ html
<link rel="stylesheet" href="{% raw %}{{ '/pub/css/bootstrap/css/bootstrap.css' | absolute_url }}{% endraw %}">
~~~

#### Message information

~~~ md
Ceci est une information
{: .alert .alert-info}
~~~

Ceci est une information
{: .alert .alert-info}

On peut changer .alert-info par .alert-warning, .alert-danger...

#### Card

~~~ html
<div class="card">
  <div class="card-header text-white bg-primary">
    Card Header
  </div>
  <div class="card-body">
    <h6 class="card-title">Card title</h6>
    <p class="card-text">Texte de la card</p>
    <a href="#" class="btn btn-primary text-white">Go somewhere</a>
  </div>
</div>
~~~

<div class="card">
  <div class="card-header text-white bg-primary">
    Card Header
  </div>
  <div class="card-body">
    <h6 class="card-title">Card title</h6>
    <p class="card-text">Texte de la card</p>
    <a href="#" class="btn btn-primary text-white">Go somewhere</a>
  </div>
</div>

## TOC

La TOC ou TableOfContent est indispensable à la navigation souhaitée pour ce site.
Jekyll propose un plugin pour gérer la TOC.

### Installation de la TOC
Les explications ci-dessous proviennent de la documentation 
[jekyll-toc](https://github.com/toshimaru/jekyll-toc)

On ajoute le plugin au fichier `_config.yml` et dans le `Gemfile`

**Attention** : Il faut toujours ajouter également les plugins au Gemfile pour la phase de déploiement.
{: .alert .alert-warning}

~~~ yml
plugins:
  - jekyll-toc
~~~

On ajoute `toc: true` dans le fichier où on va avoir une toc
~~~
---
layout: post
title: "Welcome to Jekyll!"
toc: true
---
~~~

Et ensuite, on modifie le layout où on veut voir apparaitre la TOC

Exemple, dans `_layout/post.html`

~~~ html
<div>
    <div id="table-of-contents">
      {% raw %}{{ content | toc_only }}{% endraw %}
    </div>
    <div id="markdown-content">
     {% raw %}{{ content | inject_anchors }}{% endraw %}
    </div>
  </div>
~~~

### Customiser la TOC
On peut ensuite customiser la TOC.
* Réduire les niveaux de TOC.

  Dans `_config.yml`

  ~~~ yml
  toc:
    min_level: 2 # default: 1
    max_level: 5 # default: 6
  ~~~

* Modifier le CSS 

  Dans `pub/css/custom.css`

  **Information** : J'ai créé un fichier `custom.css` pour toutes les modifications apportées au template Lanyon.
  {: .alert .alert-info}

  ~~~ css
  .section-nav {
    /* position: sticky !important; /* added to make toc scroll with page */
    /* float: right !important; added to make toc scroll with page */
    background-color: #fff;
    margin: 5px 0;
    padding: 10px 30px;
    border: 1px solid #e8e8e8;
    border-radius: 3px;
  }
  ~~~

## Les collections

Les posts, c'est bien, mais je souhaite aussi avoir des collections pour ce site.

>Les collections vont me permettent de proposer de la documentation statique sur certains sujets (à propos de mbed pour commencer) et d'avoir une entrée dans ma sidebar.

Les explications ci-dessous proviennent de la documentation officielle. [introduction-to-jekyll-collections](https://learn.cloudcannon.com/jekyll/introduction-to-jekyll-collections/)

### Ajout de la collection

On modifie `_config.yml` pour intégrer notre nouvelle collection

~~~ yml
collections:
  mbed:
    output: true
    sort_by: lesson
~~~

Ma collection s'appelle mbed. Je précise qu'elle sera trié par `lesson`, mot clé qui apparait dans le [Front Matter](https://jekyllrb.com/docs/front-matter/) de chaque page.


### Configuration de la collection
Pour configurer la collection, nous avons besoin : 
* D'un répertoire pour héberger les fichiers
* De fichiers `*.md` dans ce répertoire
* D'un fichier `mbed.html` à la racine de notre site
* D'un fichier `_layout/mbed.html`

1. Je crée un répertoire `_mbed` à la racine du site et je dépose les fichiers (mbed1.md, mdeb2.md) dans ce répertoire

1. Chacun des fichiers du répertoire contient à minima

    ~~~ md
    ---
    layout: mbed
    title: mbed1
    description : mbed1
    lesson: 1
    ---

    mbed 1
    bla blbalbalblbala
    ~~~

1. Création de mbed.html

    On crée ensuite une page qui va servir la collection.
    Elle va parcourir les fichiers de la collection puis afficher pour chacun des éléments le lien vers celui-ci et sa description.

    ~~~ html
    ---
    layout: page
    title: Documentation sur Mbed
    ---
    {% raw %}
    {% for mbed in site.mbed %}
    <div class="posts">
        <h2 class="post-title">  <a href="{{ mbed.url | absolute_url }}">
            {{ mbed.title }} </a></h2>   
        {{mbed.description}}
        <p></p>
    </div>
    {% endfor %}
    {% endraw %}
    ~~~

1. Layout des pages

    Ensuite, il suffit d'ajouter un layout specifique dans `_layouts` appelé `mbed.html`
    Ce layout sera appelé dès que le site servira une page de la collection.

    ~~~ html
    {% raw %}
    ---
    layout: default
    ---

    <div class="page">
      <h1 class="page-title">{{ page.title }}</h1>
      <!-- {{ content }} -->
      <div>
        <div id="table-of-contents">
          {{ content | toc_only }}
        </div>
        <div id="markdown-content">
          {{ content | inject_anchors }}
        </div>
      </div>
    </div>
    {% endraw %}
    ~~~

### Ascinema

J'ai déjà dit que parfois on découvre certains outils indispensables bien trop tard ?

Dans mon travail, mes étudiants doivent souvent taper quelques lignes de code dans le terminal. En début de formation, ils ne maitrisent pas celui-ci ce qui provoquent des erreurs à n'en plus finir.

[Asciinema] permet d'enregistrer et de partager facilement des sessions shells.

Une fois installé, il suffit de d'exécuter `asciinema rec` pour lancer un enregistrement, de taper les commandes voulues, puis , `CTRL^D` pour l'arrêter.

~~~ shell
asciinema rec
~~~

Ensuite, on se connecte au site asciinema et on récupère le script d'intégration, that's it !

**Exemple**

~~~js
<script id="asciicast-b4mPj8pToKMxps82uQNYgFGN4" src="https://asciinema.org/a/b4mPj8pToKMxps82uQNYgFGN4.js" async></script>
~~~

<script id="asciicast-b4mPj8pToKMxps82uQNYgFGN4" src="https://asciinema.org/a/b4mPj8pToKMxps82uQNYgFGN4.js" async></script>

> Un plugin asciinema existe pour Jekyll mais il est obsolète. (Le format du hash a changé...). En attendant une mise à jour ou une autre solution, j'intègre directement le script à la sauvage


[Bundler]: https://bundler.io/
[VsCode]: https://code.visualstudio.com/
[Liquid]: https://jekyllrb.com/docs/liquid/
[bootstrap]: https://getbootstrap.com/
[Asciinema]: https://asciinema.org/