---
layout: post
title: Site de documentation SNIR
description: Un site de ressources pour les étudiants, les collègues et tous publics
keywords: BTS SNIR programmation réseau
toc: true
---

Le site de documentation de notre BTS est publié !
Il est accessible à cette adresse [Documentation BTS SNIR](http://docs.sn-kastler.fr/)

Contrairement à ce blog, les informations sur ce site sont à destination des étudiants, sous la forme de tutoriels, de TP ou d'exercices.

