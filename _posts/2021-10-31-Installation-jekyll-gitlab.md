---
layout: post
title: Installation de Jekyll et déploiement sur Gitlab Pages
description : Post pour résumer l'installation du SSG Jekyll sur une page Gitlab
keywords : SSG, web, hébergement
toc: true
---


## Gitlab pages

Je me lève ce matin, de bonne humeur, c'est les vacances.

Commençant à procrastiner sur le web, je regarde les [SSG]. (Static Site Generator)

Je farfouille sur [Jekyll], Github, Gitlab.

Je m'aperçois que Github permet l'hébergement gratuit de site web SSG ! Comme  je préfère Gitlab, je vais voir du coté de leur solution.

Et là, je m'aperçois de quelque chose qui date déjà de plusieurs années (pourquoi toujours avoir l'impression d'être en retard de 10 trains techno !), Gitlab peut aussi héberger gratuitement des sites SSG.

Il peut même : 

- Les publier automatiquement grâce à son pipeline CI/CD
- On peut les associer à un sous domaine de son choix pour changer l'URL du site
- Il gère le HTTPS en commandant automatiquement un certificat chez Let's Encrypt

## Du coup test !

Le post ci-dessous est adapté de cette page.

[gitlab-pages-setup](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/)

Il faut : 

- Un nom de domaine (prouff.xyz dans mon cas)
- Un compte gitlab
- Un SSG (Jekyll)
- Un peu d'huile de coude

> L'avantage de Gitlab, c'est qu'ils proposent aussi d'installer leur solution sur son propre hébergement.(C'est d'ailleurs que ce nous faisons pour notre école).
>
> [Framagit] propose ainsi son propre clone pour vos projets.

### Installation de Jekyll
Plusieurs moyens pour installer Jekyll.
J'ai choisi pour ce tutoriel de créer un nouveau projet sur Gitlab à partir du template Jekyll fourni.

Une fois créé, il suffit de cloner le dépôt sur son PC.

### Configurations et modifications locales

* Dans Jekyll, ajouter le `.gitlab-ci.yaml` proposé sur Gitlab.

```yaml
image: ruby:latest

variables:
  JEKYLL_ENV: production

pages:
  script:
  - bundle install
  - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - main
```

* Editer le fichier `_config.yml` pour modifier les entrées baseurl et url. (sinon, pas de css sur la partie publiée)

```yaml
baseurl: "" 
url: "https://docs.prouff.xyz/"
```

### Sur Gitlab

* Aller dans Settings -> Pages pour ajouter le sous domaine

* Préciser le nom ici `docs.prouff.xyz`

* Il faut aussi récupérer le code de vérification fourni par Gitlab

### Sur OVH.

Mon nom de domaine est géré par OVH. donc je me connecte au compte client.

* J'ajoute un sous domaine (entrée A) avec les infos suivantes 

  ```shell
  docs.prouff.xyz.  A 	35.185.44.232
  ```

* J'ajoute une Entrée TXT avec les infos suivantes :

  ```shell
  docs.prouff.xyz. TXT 	"_gitlab-pages-verification-code.docs.prouff.xyz TXT gitlab-pages-verification-code=1230"
  ```

> Contrairement à la configuration proposée par Gitlab, je n'utilise pas l'entrée CNAME + TXT, car OVH refuse d'ajouter une  entrée TXT si un CNAME existe déjà pour le sous-domaine.

## Publication du site

Le site local est lié au dépot Gitlab

```shell
git remote -v
origin	https://gitlab.com/seprou/docs-sepro.git (fetch)
origin	https://gitlab.com/seprou/docs-sepro.git (push)
```

Quand je pousse les modifications, le code source est updaté sur le dépot et le pipeline CI/CD est automatiquement lancé. A la fin de la compilation, il génère un artifact dans le dossier public et le site est accessible à l'URL choisie ! (en https messieurs dames)


![docs.prouff.xyz]({{ 'pub/images/docs.prouff.xyz.png' | absolute_url }})


## ToDo 

Avant de conclure, la liste des choses à faire et des pistes à explorer

Il reste à publier quelque chose de plus intéressant que la page par defaut d'une installation de Jekyll.

### Thème Lanyon

En farfouillant dans les thèmes, j'ai trouvé que celui-ci était adapté à mes besoins.

[Lanyon](https://github.com/poole/lanyon)

Nikhita, sur un post de 2016 donne de bonnes idées pour configurer Jekyll.

[Jekyll Lanyon Setup](https://www.nikhita.dev/build-blog-using-github-jekyll)

[Jekyll SEO](https://www.nikhita.dev/seo-jekyll)

Vous parcourez actuellement le site personnalisé avec Lanyon en suivant notamment ses recommandations.
Si vous êtes intéressés, le dépôt Gitlab est public.

~~~ 
https://gitlab.com/seprou/docs-sepro
~~~

Dans mon travail, on héberge notre propre serveur Gitlab. Ca me donne pleins d'idées pour que nos étudiants puissent publier leur propre site web ;O


[SSG]: https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9rateur_de_site_statique
[Jekyll]: https://jekyllrb.com/
[Framagit]: https://framagit.org/users/sign_in


## Netlify

Gitlab pages ne me proposait pas un référencement correct du site.
Je l'héberge finalement sur [netlify](https://www.netlify.com/)
Pour ce site, l'offre starter est bien suffisante... ;O
