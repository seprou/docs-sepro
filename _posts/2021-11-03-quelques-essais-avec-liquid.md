---
layout: post
title: Quelques essais avec Liquid
description: Liquid est le langage de templating utilisé par Jekyll
keywords: Liquid, Jekyll
toc: true
---

## Test 1 
Récupération de toutes les pages du site

~~~ liquid
{% raw %}
{% assign pages_list = site.pages | sort:"url" %}
{% for node in pages_list %}
  {% if node.layout == "page" %}
    <p> "Page : "{{ node.title }} </p>
  {% else %}
    <p> "Other" {{ node.url }}</p>
  {% endif %}  
{% endfor %}
{% endraw %}
~~~

{% assign pages_list = site.pages | sort:"url" %}
{% for node in pages_list %}
  {% if node.layout == "page" %}
    <p> "Page : "{{ node.title }} </p>
  {% else %}
    <p> "Other" {{ node.title }}</p>
  {% endif %}  
{% endfor %}

## Test 2 
Récupération de la collection `mbed`

~~~ liquid
{% raw %}
{% for mb in site.mbed %}
  <p>{{ mb.title }} </p>
  <p>{{ mb.url | absolute_url }} </p>
{% endfor %}
{% endraw %}
~~~


{% for mb in site.mbed %}
  <p>{{ mb.title }} </p>
  <p>{{ mb.url | absolute_url }} </p>
{% endfor %}


## Test 3
J'ai ajouté un élément `sidebar` dans le front matter de `mbed.html`

~~~
---
layout: page
title: Documentation sur Mbed
sidebar : master
---
~~~

Comme ceci, je peux filter en fonction de cet élément.

~~~ liquid
{% raw %}
{% for page in site.pages %}
  <p>{{ page.title }} </p>
  {%if page.sidebar contains "master" %}
  <h2>{{ page.title }} </h2>
  <p>{{ page.url | absolute_url }} </p>
  {% endif %}
{% endfor %}
{% endraw %}
~~~


{% for page in site.pages %}
  <p>{{ page.title }} </p>
  {%if page.sidebar contains "master" %}
  <p>HIT ! -> {{ page.title }} </p>
  <p>HIT ! -> {{ page.url | absolute_url }} </p>
  {% endif %}
{% endfor %}

## Configuration de la sidebar

J'ai utilisé le plugin nagvoco.
Pour avoir le menu déroulant, la sidebar contient désormais...

~~~ 
{% raw %}
 {% assign pages_list = site.pages | sort:"url" %}
    {% for node in pages_list %}
      {% if node.title != null %}
        {% if node.layout == "page" %}
          {%if node.sidebar contains "master" %}
            <ul class="nav sidebar-item">
              <li><a href="#">{{ node.title }}</a>
                <ul>
                  {% assign mbed_sort_lesson = site.mbed | sort: "lesson" %}
                  {% for mb in mbed_sort_lesson %}
                  <li><a class="sidebar-nav-item{% if page.url == node.url %} active{% endif %}" href="{{ mb.url | absolute_url }}">{{ mb.title }}</a></li>
                  {% endfor %}
                </ul>
              </li>
            </ul>
          {% else %}
            <a class="sidebar-nav-item{% if page.url == node.url %} active{% endif %}" href="{{ node.url | absolute_url }}">{{
            node.title }}</a>
          {% endif %}
        {% endif %}
      {% endif %}
    {% endfor %}
{% endraw %}
~~~



